/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.perimeter;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.servlet.FileServerServlet;
import com.atlassian.renderer.WikiStyleRenderer;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.randombits.confluence.support.ConfluenceMacro;
import org.randombits.confluence.support.LinkAssistant;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.storage.Storage;

/**
 * Created by IntelliJ IDEA.
 * User: david
 * Date: Dec 10, 2005
 * Time: 1:52:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class SecureIncludeMacro extends ConfluenceMacro
{
    private static final String ID = "id";
    public static final String ID_PARAM = "secureIncludeId";
    public static final String LINK_PARAM = "secureIncludeLink";
    public static final String PAGE_ID_PARAM = "pageId";

    private ContentEntityManager contentEntityManager;
    private LinkAssistant linkAssistant;
    private WikiStyleRenderer wikiStyleRenderer;
    private PermissionManager permissionManager;
    private UserAccessor userAccessor;
    private BootstrapManager bootstrapManager;

    @Override
    protected String execute(MacroInfo info) throws MacroException
    {
        String id = info.getMacroParams().getString(ID, null);
        if (id == null)
            throw new MacroException("Please supply an id which is unique to this page.");

        SecureIncludeData data = SecureIncludeData.load(info.getContent(), id);

        if (data == null)
        {
            data = processRequest(id, info);
        }

        if (data != null)
        {
            return includePage(id, data, info);
        }
        else
        {
            User user = info.getCurrentUser();

            if (permissionManager.hasPermission(user, Permission.EDIT, info.getContent()))
                return inputForm(id, info);

            return "";
        }
    }

    private String inputForm(String id, MacroInfo info)
    {
        Storage reqParams = info.getRequestParams();

        String link = reqParams.getString(LINK_PARAM, null);
        String ctxPath = info.getPageContext().getSiteRoot();

        StringBuffer out = new StringBuffer();

        out.append("<div style='border: 1px dashed gray'>");
        out.append("<form name=\"SecureIncludeForm\" method=\"post\" action=\"")
            .append(ctxPath).append("/pages/viewpage.action?pageId=")
            .append(info.getContent().getId()).append("\">\n");

        out.append("<input type=\"hidden\" name=\"").append(ID_PARAM)
            .append("\" value=\"").append(GeneralUtil.htmlEncode(id)).append("\"/>\n");

        if (link != null)
            out.append("<div class='error'>The specified link does not exist or is not accessible.</div>\n");

        out.append("<p>");
        out.append("Enter the link to any other page you can view, in any space, and anyone who can view <i>this</i> page will be able to view it.<br/>");
        out.append("<b>Link:</b> <input type='text' name='").append(LINK_PARAM)
            .append("' value=\"").append((link == null) ? "" : link).append("\" width='20'/>");
        out.append(" <input type='submit' name='go' value='Include'/>");
        out.append("</p>\n");

        out.append("</form>");
        out.append("</div>");

        return out.toString();
    }

    private SecureIncludeData processRequest(String id, MacroInfo info)
    {
        Storage reqParams = info.getRequestParams();

        if (id.equals(reqParams.getString(ID_PARAM, null)))
        {
            String link = reqParams.getString(LINK_PARAM, null);
            if (link != null && link.length() > 0)
            {
                // Try to find the content.
                ConfluenceEntityObject entity = getLinkAssistant().getLinkedEntity(info.getPageContext(), link);
                if (entity instanceof ContentEntityObject)
                {
                    User user = info.getCurrentUser();
                    if (permissionManager.hasPermission(user, Permission.VIEW, entity))
                    {
                        SecureIncludeData data = new SecureIncludeData(user.getName(), entity.getId());
                        SecureIncludeData.save(info.getContent(), id, data);
                        return data;
                    }
                }
            }
        }
        return null;
    }

    private String includePage(String id, SecureIncludeData data, MacroInfo info) throws MacroException
    {
        ContentEntityObject content = contentEntityManager.getById(data.getContentId());

        if (content == null)
            throw new MacroException("The content this secure include accesses no longer exists.");

        User accessor = userAccessor.getUser(data.getUsername());
        if (accessor == null)
            throw new MacroException("The user who set up this secure include no longer exists: " + data.getUsername());

        if (!permissionManager.hasPermission(accessor, Permission.VIEW, content))
            throw new MacroException("The user who set up this secure include no longer has access to the resource.");

        PageContext ctx = content.toPageContext();

        String contentIncludeId = SecureAttachmentDownload.ATTACHMENT_PATH + "/" + content.getId() + "/" + info.getContent().getId() + "/" + GeneralUtil.urlEncode(id);

        String securePath = bootstrapManager.getWebAppContextPath() + contentIncludeId;

        ctx.setSiteRoot(info.getPageContext().getSiteRoot());
        ctx.setBaseUrl(info.getPageContext().getBaseUrl());
        ctx.setImagePath(info.getPageContext().getImagePath());
        ctx.setAttachmentsPath(securePath);

        // Fake logging in as the original accessor...
        User currentUser = AuthenticatedUserThreadLocal.getUser();
        AuthenticatedUserThreadLocal.setUser(accessor);

        //return subRenderer.render(content.getContent(), ctx, RenderMode.ALL);
        String rendered = wikiStyleRenderer.convertWikiToXHtml(ctx, content.getContent());

        // Return to the real user...
        AuthenticatedUserThreadLocal.setUser(currentUser);

        rendered = replaceAttachmentUrls(rendered, data, contentIncludeId);

        return rendered;
    }

    private String replaceAttachmentUrls(String rendered, SecureIncludeData data, String contentIncludeId)
    {
        String attachmentUrl = "/" + FileServerServlet.SERVLET_PATH + "/" + FileServerServlet.ATTACHMENTS_URL_PREFIX + "/" + data.getContentId();
        return rendered.replaceAll(attachmentUrl, contentIncludeId);
    }

    @Override
    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    public void setWikiStyleRenderer(WikiStyleRenderer wikiStyleRenderer)
    {
        this.wikiStyleRenderer = wikiStyleRenderer;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    protected LinkAssistant getLinkAssistant()
    {
        if (linkAssistant == null)
            linkAssistant = LinkAssistant.getInstance();
        return linkAssistant;
    }

    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
}
